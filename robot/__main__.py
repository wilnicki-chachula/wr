from robot.jobs.cargo_job import CargoJob

job = CargoJob(
    period=0,
    config_file='config.ini',
    motor_right_port='outA',
    motor_left_port='outC',
    motor_center_port='outD',
    sensor_left_port='in4',
    sensor_right_port='in1',
    sensor_ir='in3',
)

# job = LineFollowerJob(
#     period=1e-8,
#     config_file='config.ini',
#     motor_right_port='outA',
#     motor_left_port='outC',
#     sensor_left_port='in4',
#     sensor_right_port='in1',
# )

job.start()
