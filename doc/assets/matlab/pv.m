syms x m M d;

% PARAMETRY (mozna sie bawic i patrzec co sie dzieje)

% dziedzina x-ow, w praktyce powinno byc [-1, 1], ale dalem wiecej zeby
% bylo widac co sie dzieje jak zjezdza z linii
D = [-3, 3];

% odleglosc miedzy czujnikami
dist = 0.95;

% co zwraca czujnik gdy widzi tylko biale/czarne
% vmax = 1;
% vmin = 0;
vmax = 0.75;
vmin = 0.1;


% modele czujnikow
f = @(x, m, M) ((M - m) * sin((x - 1/2) * pi) + M + m) / 2;
R = @(x, m, M, d) indicator(x, [-2 * d - 1 + d, 1 - d]) * f(x + d, vmin, vmax) + indicator(x, [-inf, -2 * d - 1 + d]) * M + indicator(x, [1 - d, inf]) * M;
L = @(x, m, M, d) indicator(x, [d - 1, 2 * d + 1 - d]) * f(x - d, vmin, vmax)  + indicator(x, [-inf, d - 1]) * M + indicator(x, [2 * d + 1 - d, inf]) * M;
%R = @(x) min(abs(x - 1), 1);
%L = @(x) min(abs(x + 1), 1);

% jak liczymy PV
PV = @(x) R(x, vmin, vmax, dist) - L(x, vmin, vmax, dist);

% rysowanie wykresu
figure;
hold on;
ylim([-0.2, 0.9]);
grid;
% title(strcat('Położenie robota względem linii dla d=', num2str(dist)));
xlabel('rzeczywiste względne położenie robota');
% r0 = line([-dist, -dist], [-1, 1], 'Color', [0.25 0.25 0.25], 'LineStyle', '--');
% r1 = line([dist, dist], [-1, 1], 'Color', [0.25 0.25 0.25], 'LineStyle', '--');
r = fplot(@(x) R(x, vmin, vmax, dist), D, 'r');
l = fplot(@(x) L(x, vmin, vmax, dist), D, 'b');
% p = fplot(PV, D, 'k', 'DisplayName', 'PV');
% legend([r0 r l p], {'zakres pracy', 'czujnik prawy', 'czujnik lewy', 'obliczone położenie robota'}, 'Location','southeast');
legend([r l], {'czujnik prawy', 'czujnik lewy'}, 'Location','southeast');
% saveas(gcf, strcat('../img/pv_', strrep(num2str(dist), '.', '_'), '.png'));
hold off;