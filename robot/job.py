import configparser
import sys
from abc import abstractmethod
from ast import literal_eval
from threading import Thread

from robot.line_follower_pid_controller import LineFollowerPidController
from robot.utilities import PeriodicTimer


class Job:
    def __init__(self, period, config_file):
        self.controller = LineFollowerPidController()
        self.controller_thread = Thread(target=self.process_controller)

        self.coeffs = {key: 0 for key in ['k_p', 'k_i', 'k_d', 'speed_{max}', 'k_{sr}']}
        self.io = {}

        self._config_file = config_file
        self._config = configparser.ConfigParser()

        # self.input_thread = Thread(target=self._process_input)
        # self.input_thread.start()

    def process_controller(self):
        while True:
            self.tick()

    @abstractmethod
    def tick(self):
        return

    def start(self):
        self._config_read()
        self._config_write()
        self.controller_thread.start()

    def stop(self):
        pass

    def _run_motor_diff(self, motor, out, sr=0):
        speed = (1 - sr) * self.coeffs['speed_{max}'] - out
        if speed < -1:
            # print('Warning: speed < -1\t\tspeed = {}'.format(speed), file=sys.stderr)
            speed = -1
        elif speed > 1:
            # print('Warning: speed > 1\t\tspeed = {}'.format(speed), file=sys.stderr)
            speed = 1

        motor.run_forever(speed_sp=1050 * speed)

    def _config_write(self):
        try:
            self._config.add_section('coeffs')
        except configparser.DuplicateSectionError:
            pass

        for key, value in self.coeffs.items():
            self._config['coeffs'][key] = str(value)

        with open(self._config_file, 'w') as f:
            self._config.write(f)

        print({'coeffs': self.coeffs})

    def _config_read(self):
        self._config.read(self._config_file)

        if self._config.has_section('coeffs'):
            for key, value in self._config['coeffs'].items():
                self.coeffs[key] = float(value)

    def _process_input(self):
        for line in sys.stdin:
            data = literal_eval(line)

            if 'coeffs' in data:
                self.coeffs.update(data['coeffs'])

            if data.get('exit', False):
                self.stop()

            self._config_write()
