from ev3dev.core import ColorSensor, LargeMotor

from robot.job import Job
from robot.utilities import rgb2hsv, normalize_rgb


class LineFollowerJob(Job):
    def __init__(self, period, config_file, motor_right_port, motor_left_port, sensor_left_port, sensor_right_port):
        super().__init__(period, config_file)

        self.io['m_l'] = LargeMotor(motor_left_port)

        self.io['m_r'] = LargeMotor(motor_right_port)

        self.io['s_l'] = ColorSensor(sensor_left_port)
        self.io['s_l'].auto_mode = False
        self.io['s_l'].mode = ColorSensor.MODE_RGB_RAW

        self.io['s_r'] = ColorSensor(sensor_right_port)
        self.io['s_r'].auto_mode = False
        self.io['s_r'].mode = ColorSensor.MODE_RGB_RAW

        self.hsv = {'l': 0, 'r': 0}

    def tick(self):
        pv = self.pv()

        out, sr, [p, i, d] = self.controller.next(pv,
                                                  {key: self.coeffs[key] for key in ['k_p', 'k_i', 'k_d', 'k_{sr}']})

        if out > 0:  # we need to turn right
            self._run_motor_diff(self.io['m_l'], 0, sr)
            self._run_motor_diff(self.io['m_r'], out, sr)
        else:  # we need to turn left
            self._run_motor_diff(self.io['m_l'], -out, sr)
            self._run_motor_diff(self.io['m_r'], 0, sr)

        print({
            'pv': pv,
            'sr': sr,
            'p': p,
            'i': i,
            'd': d,
        })

    def pv(self):
        self.hsv['l'] = rgb2hsv(normalize_rgb(self.io['s_l'].raw))
        self.hsv['r'] = rgb2hsv(normalize_rgb(self.io['s_r'].raw))

        return self.hsv['r'][2] - self.hsv['l'][2]
