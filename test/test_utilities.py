import unittest

from robot.utilities import rgb2hsv


class TestUtilities(unittest.TestCase):

    def test_rgb2hsv(self):
        rgb_data = [
            [52, 79, 151],
            [76, 235, 66],
            [120, 109, 153],
            [58, 47, 181],
            [215, 230, 56],
            [49, 249, 29],
            [57, 111, 75],
            [43, 28, 81],
            [58, 65, 108],
            [111, 104, 129],
        ]

        hsv_data = [
            [0.6212, 0.6556, 0.5922],
            [0.3235, 0.7191, 0.9216],
            [0.7083, 0.2876, 0.6000],
            [0.6803, 0.7403, 0.7098],
            [0.1810, 0.7565, 0.9020],
            [0.3182, 0.8835, 0.9765],
            [0.3889, 0.4865, 0.4353],
            [0.7138, 0.6543, 0.3176],
            [0.6433, 0.4630, 0.4235],
            [0.7133, 0.1938, 0.5059],
        ]

        for rgb, hsv in zip(rgb_data, hsv_data):
            (h, s, v) = rgb2hsv(x / 255 for x in rgb)
            self.assertAlmostEqual(h, hsv[0], places=4)
            self.assertAlmostEqual(s, hsv[1], places=4)
            self.assertAlmostEqual(v, hsv[2], places=4)


if __name__ == '__main__':
    unittest.main()
