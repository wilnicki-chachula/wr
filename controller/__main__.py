import subprocess
from ast import literal_eval
from queue import Queue
from threading import Thread

from controller.RollPlot import RollPlot

robot = subprocess.Popen(
    ['ssh', 'robot@10.42.0.69', 'cd ~/cw && python3 -u -m robot'],
    # ['ssh', 'protecto@localhost', 'cd ~/PycharmProjects/wr_tbd && python3 -u -m robot'],
    bufsize=1,
    stdin=subprocess.PIPE,
    stdout=subprocess.PIPE,
    shell=False,
    universal_newlines=True,
)


def enqueue_output(out, queue):
    for line in iter(out.readline, b''):
        data = literal_eval(line)
        queue.put(data)
    out.close()


q = Queue()
t = Thread(target=enqueue_output, args=(robot.stdout, q))
t.daemon = True
t.start()

RollPlot.init(
    line_names=['pv', 'sr', 'p', 'i', 'd'],
    param_names=['k_p', 'k_i', 'k_d', 'speed_{max}', 'k_{sr}', 'h_{gl}', 'h_{gr}', 'h_{rl}', 'h_{rr}', 's_{gl}',
                 's_{gr}', 's_{rl}', 's_{rr}', 'sp_{turn}'],
    # line_names=['pv', 'sr', 'p', 'i', 'd'],
    # param_names=['k_p', 'k_i', 'k_d', 'speed_{max}', 'k_{sr}'],

    robot_stdin=robot.stdin,
    queue=q,
    samples_visible=round(1 / 0.05 * 5)
)

robot.stdin.write(str({'exit': True}) + '\n')
