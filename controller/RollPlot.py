from queue import Empty

from matplotlib import pyplot as plt, gridspec
from matplotlib.animation import FuncAnimation
from matplotlib.widgets import TextBox, CheckButtons, Button


class RollPlot:
    line_names = None
    param_names = None
    robot_stdin = None
    queue = None
    samples_visible = None
    xdata = None
    ydata = None
    figs = None
    last_param_values = None
    data_points = 0

    @classmethod
    def init(cls, line_names, param_names, robot_stdin, queue, samples_visible):
        cls.line_names = line_names
        cls.param_names = param_names
        cls.robot_stdin = robot_stdin
        cls.queue = queue
        cls.samples_visible = samples_visible

        cls.xdata = [None for _ in range(600)]
        cls.ydata = {key: [None for _ in range(600)] for key in line_names}
        cls.figs = {key: {'fig': plt.figure()} for key in ['scope', 'settings']}
        cls.last_param_values = {key: 0 for key in param_names}
        cls.last_samples_visible = samples_visible

        cls._init_scope()
        cls._init_settings()

        plt.show()

    @classmethod
    def _init_scope(cls):
        scope = cls.figs['scope']
        scope['ax'] = scope['fig'].add_subplot(111)
        scope['lines'] = {key: scope['ax'].plot([0, 1, 2], [0.5, 0.5, 0.5], label=key, animated=True)[0] for key in
                          cls.line_names}
        scope['ax'].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                           ncol=3, mode="expand", borderaxespad=0.)
        [line.set_visible(False) for line in scope['lines'].values()]
        scope['ax'].get_xaxis().set_visible(False)
        scope['ax'].set_ylim(-1, 1)
        scope['ax'].grid(True)
        cls.animation = FuncAnimation(cls.figs['scope']['fig'], RollPlot._update, blit=True, interval=1000 / 60)

    @classmethod
    def _init_settings(cls):
        settings = cls.figs['settings']
        gs = gridspec.GridSpec(len(cls.param_names) + 2, 4)
        settings['samples_visible'] = TextBox(settings['fig'].add_subplot(gs[0, 1]), 'samples_visible',
                                              initial=str(cls.samples_visible))

        def samples_visible_submit(text):
            try:
                cls.samples_visible = int(text)
                cls.last_samples_visible = text
            except ValueError:
                settings['samples_visible'].set_val(cls.last_samples_visible)

        settings['samples_visible'].on_submit(samples_visible_submit)

        settings['clear'] = Button(settings['fig'].add_subplot(gs[0, 2]), 'Clear')

        def on_clear_clicked(event):
            cls.xdata = [None for _ in range(600)]
            cls.ydata = {key: [None for _ in range(600)] for key in cls.line_names}

        settings['clear'].on_clicked(on_clear_clicked)

        settings['params'] = {key: TextBox(settings['fig'].add_subplot(gs[idx + 2, 1]), '${}$'.format(key)) for
                              idx, key in enumerate(cls.param_names)}

        def submit(key, value):
            if cls.last_param_values[key] == value:
                return
            try:
                cls.robot_stdin.write(str({'coeffs': {key: float(value)}}) + '\n')
                cls.last_param_values[key] = value
            except ValueError:
                cls.figs['settings']['params'][key].set_val(cls.last_param_values[key])

        def register_on_submit(key):
            settings['params'][key].on_submit(lambda text: submit(key, text))

        [register_on_submit(key) for key in settings['params'].keys()]
        settings['lines_visible'] = CheckButtons(settings['fig'].add_subplot(gs[2:, 2]), cls.line_names,
                                                 [False for _ in cls.line_names])
        settings['lines_visible'].on_clicked(
            lambda key: cls.figs['scope']['lines'][key].set_visible(not cls.figs['scope']['lines'][key].get_visible()))

    @classmethod
    def _push_data(cls, data):
        cls.xdata = [cls.data_points] + cls.xdata[:-1]
        cls.ydata = {key: [data[key]] + ydata[:-1] for key, ydata in cls.ydata.items()}
        cls.data_points += 1

    @classmethod
    def _update(cls, frame=None):
        while True:
            try:
                data = cls.queue.get_nowait()
                if sorted(list(data.keys())) == sorted(cls.line_names):
                    cls._push_data(data)
                elif sorted(list(data['coeffs'].keys())) == sorted(cls.param_names):
                    [cls.figs['settings']['params'][key].set_val(value) for key, value in data['coeffs'].items()]
            except Empty:
                break
        [line.set_data(cls.xdata, cls.ydata[key]) for key, line in cls.figs['scope']['lines'].items()]
        cls.figs['scope']['ax'].set_xlim(cls.data_points - cls.samples_visible - 1, cls.data_points - 1)
        return cls.figs['scope']['lines'].values()
