from robot.job import Job
from robot.utilities import FixedSizeList


class LineFollowerMockJob(Job):
    def __init__(self, period, config_file):
        super().__init__(period, config_file)

        self.coeffs.setdefault('k', 0)
        self.coeffs.setdefault('t', 0)

        self.pvs = FixedSizeList([0, 0])

    def tick(self):
        pv = self.pv()

        out, sr, [p, i, d] = self.controller.next(pv,
                                                  {key: self.coeffs[key] for key in ['k_p', 'k_i', 'k_d', 'k_{sr}']})

        self.pvs.push(self.coeffs['k'] * out - self.coeffs['t'] * (self.pvs[0] - self.pvs[1]) - 1)

        print({
            'pv': pv,
            'sr': sr,
            'p': p,
            'i': i,
            'd': d,
        })

    def pv(self):
        return self.pvs[0]
