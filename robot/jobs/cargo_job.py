import time
from sys import stderr

from ev3dev.core import InfraredSensor, MediumMotor

from robot.jobs.line_follower_job import LineFollowerJob


class CargoJob(LineFollowerJob):
    STATES = [
        'search_for_exit',
        'exiting',
        'search_for_platform',
        'ready_to_rotate',
        'rotating',
        'search_for_entry',
        'entering',
        'delivery',
    ]

    def __init__(self, period, config_file, motor_right_port, motor_left_port, motor_center_port, sensor_left_port,
                 sensor_right_port,
                 sensor_ir):
        super().__init__(period, config_file, motor_right_port, motor_left_port, sensor_left_port, sensor_right_port)

        self.coeffs.update({key: 0 for key in [
            'h_{gl}', 'h_{gr}', 'h_{rl}', 'h_{rr}', 's_{gl}', 's_{gr}', 's_{rl}', 's_{rr}', 'sp_{turn}',
        ]})

        self.io['m_c'] = MediumMotor(motor_center_port)

        self.io['s_c'] = InfraredSensor(sensor_ir)
        self.io['s_c'].auto_mode = False
        self.io['s_c'].mode = InfraredSensor.MODE_IR_PROX

        self.state = 'search_for_exit'
        self.has_cargo = False
        self.lf_active = True
        self.turn_time = 0
        self.reversing_time = 0
        self.rotate_position = 0
        self.prox = 1

    def start(self):
        super().start()
        self.io['m_c'].run_to_abs_pos(position_sp=0, speed_sp=1050, stop_action='hold')

    def tick(self):
        pv = self.pv()

        out, sr, _ = self.controller.next(pv, {key: self.coeffs[key] for key in ['k_p', 'k_i', 'k_d', 'k_{sr}']})

        if self.lf_active:
            if out > 0:  # we need to turn right
                pass
                self._run_motor_diff(self.io['m_l'], 0, sr)
                self._run_motor_diff(self.io['m_r'], out, sr)
            else:  # we need to turn left
                pass
                self._run_motor_diff(self.io['m_l'], -out, sr)
                self._run_motor_diff(self.io['m_r'], 0, sr)

        crossroads_detected = self.detect_crossroads()
        cargo_detected = self.detect_cargo()
        platform_detected = self.detect_platform()

        if self.state == 'search_for_exit':
            if not self.has_cargo and crossroads_detected == 1:
                self.controller.setpoint = self.coeffs['sp_{turn}']
                self.turn_time = time.time()
                self.state = 'exiting'
            if self.has_cargo and crossroads_detected == 2:
                self.controller.setpoint = self.coeffs['sp_{turn}']
                self.turn_time = time.time()
                self.state = 'exiting'

        if self.state == 'exiting':
            if time.time() - self.turn_time > 0.75:
                self.controller.setpoint = 0
                self.state = 'search_for_platform'

        if self.state == 'search_for_platform':
            self.prox = self.io['s_c'].proximity / 100
            if not self.has_cargo and cargo_detected:
                self.lf_active = False
                self.io['m_l'].run_forever(speed_sp=0)
                self.io['m_r'].run_forever(speed_sp=0)
                self.io['m_c'].run_to_abs_pos(position_sp=-35, speed_sp=1050, stop_action='hold')
                self.has_cargo = True
                self.state = 'ready_to_rotate'
            elif self.has_cargo and platform_detected:
                self.lf_active = False
                self.io['m_l'].run_forever(speed_sp=0)
                self.io['m_r'].run_forever(speed_sp=0)
                self.io['m_c'].run_to_abs_pos(position_sp=0, speed_sp=1050, stop_action='hold')
                self.has_cargo = False
                self.state = 'delivery'

        if self.state == 'ready_to_rotate':
            if self.has_cargo and self.io['m_c'].position < -26:
                self.rotate_position = self.io['m_l'].position
                self.io['m_l'].run_to_rel_pos(position_sp=535, speed_sp=260, stop_action='brake')
                self.io['m_r'].run_to_rel_pos(position_sp=-535, speed_sp=260, stop_action='brake')
                self.state = 'rotating'
            if not self.has_cargo and time.time() - self.reversing_time > 0.5:
                self.rotate_position = self.io['m_l'].position
                self.io['m_l'].run_to_rel_pos(position_sp=535, speed_sp=260, stop_action='brake')
                self.io['m_r'].run_to_rel_pos(position_sp=-535, speed_sp=260, stop_action='brake')
                self.state = 'rotating'

        if self.state == 'rotating':
            if self.io['m_l'].position - self.rotate_position >= 530:
                self.lf_active = True
                self.state = 'search_for_entry'

        if self.state == 'search_for_entry':
            if self.hsv['l'][2] < 0.25 and self.hsv['r'][2] < 0.25:
                self.controller.setpoint = self.coeffs['sp_{turn}']
                self.turn_time = time.time()
                self.state = 'entering'

        if self.state == 'entering':
            if time.time() - self.turn_time > 0.75:
                self.controller.setpoint = 0
                self.state = 'search_for_exit'

        if self.state == 'delivery':
            if self.io['m_c'].position > -5:
                self.reversing_time = time.time()
                self.io['m_l'].run_forever(speed_sp=-150)
                self.io['m_r'].run_forever(speed_sp=-150)
                self.prox = 1
                self.state = 'ready_to_rotate'

    def detect_crossroads(self):
        if self.is_green(self.hsv['r'], self.coeffs['h_{gr}'], self.coeffs['s_{gr}']):
            return 1
        if self.is_red(self.hsv['r'], self.coeffs['h_{rr}'], self.coeffs['s_{rr}']):
            return 2
        return 0

    def detect_cargo(self):
        if self.prox < 0.001:
            return True
        return False

    def detect_platform(self):
        if self.is_red(self.hsv['l'], self.coeffs['h_{rl}'], self.coeffs['s_{rl}']) and self.is_red(
                self.hsv['r'], self.coeffs['h_{rr}'], self.coeffs['s_{rr}']):
            return True
        return False

    def is_red(self, hsv, hue_tolerance, saturation_min):
        [h, s, v] = hsv
        return abs(h - 0.5) > 0.5 - hue_tolerance and s > saturation_min

    def is_green(self, hsv, hue_tolerance, saturation_min):
        [h, s, v] = hsv
        return abs(h - 1 / 3) < hue_tolerance and s > saturation_min
