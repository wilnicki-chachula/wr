from collections import UserList
from threading import Thread, Event


class PeriodicTimer(Thread):
    """
    A Timer that executes its target with a specific period.
    """

    def __init__(self, period, group=None, target=None, name=None, args=(), kwargs=None, *, daemon=None):
        super().__init__(group, target, name, args, kwargs, daemon=daemon)
        self.period = period
        self._stopped = Event()

    def run(self):
        while (self.period == 0 or (
                self.period > 0 and not self._stopped.wait(self.period))) and not self._stopped.is_set():
            self._target()

    def stop(self):
        self._stopped.set()


class FixedSizeList(UserList):
    def __init__(self, source_list=None):
        super().__init__()
        if source_list is None:
            source_list = []
        self.data = source_list

    def push(self, element):
        self.data = [element] + self.data[:-1]


def normalize_rgb(rgb):
    return [x / 255 for x in rgb]


def rgb2hsv(rgb):
    """
    Converts a color from RGB representation to HSV representation.
    :param rgb: an iterable of three values in the range of [0, 1]
    :return: an iterable of three values in the range of [0, 1]
    """
    [r, g, b] = rgb

    c_max = max(r, g, b)
    c_min = min(r, g, b)
    delta = c_max - c_min

    if delta == 0:
        h = 0
    elif c_max == r:
        h = (((g - b) / delta) % 6) / 6
    elif c_max == g:
        h = (((b - r) / delta) + 2) / 6
    else:
        h = (((r - g) / delta) + 4) / 6

    if c_max == 0:
        return h, 0, c_max

    return h, delta / c_max, c_max
