from robot.utilities import FixedSizeList


class LineFollowerPidController:
    def __init__(self):
        self.setpoint = 0
        self._errors = FixedSizeList([0 for _ in range(10)])

    def next(self, pv, coeffs):
        self._errors.push(self.setpoint - pv)

        p = coeffs['k_p'] * self._errors[0]

        i = 0
        for error in self._errors:
            i += coeffs['k_i'] * error

        d = coeffs['k_d'] * (self._errors[0] - self._errors[1])

        out = p + i + d

        sr = sum(error * coeffs['k_{sr}'] for error in self._errors)

        return out, sr, [p, i, d]
